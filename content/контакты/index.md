---
title: Запись на занятия
messageMinLength: 15
---
Я хочу представиться и показать небольшой фрагмент онлайн-урока по математике. На уроке возможно использование онлайн-доски, на которой могут работать одновременно и учитель и ученик.

<div id="player"></div>

<script>
   var player = new Playerjs({id:"player", file:"https://youtu.be/DfHOTJnORZU"});
</script>

<b>Оставьте свой номер — я перезвоню!</b> :telephone_receiver:

{{< contact-form >}}
